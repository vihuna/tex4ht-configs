Simplekagram configuration file for Tex4ht
==========================================

Description
-----------

Basic **Tex4ht** configurations for HTML5 conversion using Milligram framework,
with SVG images or KaTeX to display math formulas. It's designed
for small and medium-sized LaTeX articles, with the whole document placed in a
single HTML page. See the main page of the project for more information.

- It uses **Flexbox** CSS for a responsive column layout.
- Table of contents located in left Flexbox column, avoiding annoying HTML
  iframes.
- At this time, by default, math formulas are converted into SVG images.


**Required LaTeX Packages**:  `etoolbox`, `environ` and `expl3`.

**Main Goal**: this configuration tries to adapt **Tex4ht** output to ensure
compatibility with KaTeX, so there is no need to modify the original
LaTeX document manually. To do this (with `katex-*` options), this configuration
transforms the necessary **LaTeX** math commands into other macros compatible
with the javascript library, maybe at the cost of losing some features.


Options
-------

### Custom options

- `katex-local`: It uses a local copy o KaTeX, inside the folder with name
  `katex`
- `katex-CDN`: It uses the KaTeX remote CDN.
- `TOC-sticky`: Sticky CSS position for the table of contents.

### Tex4ht options

- All pagination Tex4ht options are considered incompatible with this
  configuration file; unexpected results can be obtained.
- Command line options for other document formats (html4, docbook, ...) instead
  of HTML5 will take no effect.
- Command line options for other image formats (png, gif) instead of SVG will
  not work.


Example of use: 

    make4ht -u -c simple -o output mydoc.tex "katex-local,TOC-sticky"


