Tex4ht Configurations
=====================

Description
-----------

Some configuration files with CSS styles for HTML/EPUB conversion using
TeX4ht/Make4ht/TeX4ebook.

All configurations are intended for small and medium-sized LaTeX articles; so
the whole document is placed in one HTML page.
A bunch of external projects have been used in the configurations, like 
Milligram CSS, Bootstrap, KaTeX, MathJax, STIX fonts or some TeX4ht helpers.


- Simplekagram: Basic Tex4ht configurations for HTML5 conversion using
  Milligram framework, with SVG images or KaTeX to display math formulas.

  Example of use: make4ht -u -c simple -o output mydoc.tex

- ~~Sparednoexpense: Tex4ht configurations to get a modern responsive HTML5
  document, keeping math formulas in LaTeX format and using MathJax to
  display them.~~

- ~~epub: Simple configurations for epub conversion using Tex4ebook and KaTeX.~~

- ~~pelican: Basic Tex4ht configuration with file metadata header ready for
  Pelican.~~


Resources
---------

- [Milligram CSS](https://milligram.io): minimal CSS flexbox framework, under
  the MIT license.

- [Michal Hoftich helpers4ht](https://github.com/michal-h21/helpers4ht):
  collection of tex4ht packages for tex4ht configuration, presumably under
  LPPLv1.3 license.

- [KaTeX](https://katex.org): math typesetting library for the web, under MIT
  license.

- [STIX fonts](https://www.stixfonts.org/): OpenType Unicode fonts for
  Scientific, Technical, and Mathematical texts; SIL Open Font License v1.1.

- [Bootstrap](https://getbootstrap.com): responsive, mobile-first,  HTML+CSS+JS
  framework; MIT license.

- [MathJax](): a JavaScript display engine for mathematics that works in all
  browsers; under Apache License.



License
-------

All authored TeX4ht configurations in this project are licensed under the MIT license.
The other external projects included here have their own licenses. 
